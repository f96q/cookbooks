Chef::Log.level = :debug

include_recipe 'chef_handler'

chef_gem 'chef-handler-profiler'

require 'chef/handler/chef_profiler'

chef_handler 'Chef::Handler::Profiler' do
  source 'chef/handler/chef_profiler'
  action :nothing
end.run_action(:enable)
